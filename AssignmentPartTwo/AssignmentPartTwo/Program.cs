﻿using System;

namespace OpdrachtTwee
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int vatPercentage = 21;
            string password = "admin";
            bool adminLogin = false;
            string horizontalLine = new string('-', 45);
            bool exitProgram = false;
            string shopName = "Boardroom Elegance";
            string userName;

            string[] productNames =
            {
                "Wooting 60HE",
                "Apex Pro",
                "Razer Huntsman V2",
                "Keychron Q1",
                "iQunix ZX75"
            };

            string[] productDetails =
            {
                "The Wooting 60HE is an analog mechanical keyboard with <1ms input response.",
                "The Apex Pro is the world's fastest gaming keyboard with Rapid Trigger for incredibly fast activation from SteelSeries.",
                "The Huntsman V2 from Razer is an optical keyboard with up to true 8000 Hz polling rate.",
                "The Q1 is a revolutionary all metal keyboard with customizable capabilities for every switch, keycap, stabilizer, knob, and even the plate.",
                "The ZX75 is a mechanical keyboard featuring high-quality mechanical switches that deliver satisfying tactile feedback and lightning-fast response times."
            };

            string[] productLayouts =
            {
                "ISO French",
                "ANSI United States",
                "ISO United Kingdom",
                "ISO German",
                "ANSI United States"
            };

            int[] productQuantities = { 4, 17, 8, 13, 7 };
            double[] productPrices = { 148.75, 190.07, 140.49, 133.88, 178.50 };

            Console.OutputEncoding = System.Text.Encoding.UTF8; //kan ik euro teken gebruiken

            Console.Write("Name: ");
            userName = Console.ReadLine();
            Console.Clear();
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine($"Welcome to {shopName}, {userName}!");
            Console.WriteLine("We are an online shop that sells mechanical keyboards!\n");
            Console.ResetColor();

            while (!exitProgram) //main loop
            {
                DisplayProductInfo(productNames, productQuantities); //roept de methode op die de shopping menu laat zien vanonder in visual studio
                Console.WriteLine($"\nWhat would you like to do {userName}?\n");
                Console.ResetColor();
                Console.WriteLine("Option 1: View a Product");
                Console.WriteLine("Option 2: Purchase a Product");
                Console.ForegroundColor = ConsoleColor.Red;
                if (!adminLogin) //als niet ingelogt is dan toont die login, anders toont die logout
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Option 3: Admin Login");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Option 3: Admin Logout");
                    Console.ResetColor();
                }
                Console.WriteLine("Option 4: Exit");
                if (adminLogin) //als er ingelogt is dan geeft die 5de optie
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Option 5: Refill a Product");
                    Console.ResetColor();
                }
                Console.WriteLine("\nAnswer the following questions with the corresponding number.");
                Console.Write("Choose an option: ");
                int choice = Convert.ToInt32(Console.ReadLine());

                #region begin code voor optie 1 (view producten)
                if (choice == 1)
                {
                    Console.Write("Which product would you like to view: ");
                    int secondChoice = Convert.ToInt32(Console.ReadLine());
                    secondChoice--;

                    if (secondChoice >= 0 && secondChoice < productNames.Length) //als user een input geeft tussen 1 en 5 (0-4 index)
                    {
                        Console.Clear();
                        if (productQuantities[secondChoice] <= 0) //als hoeveelheid <= aan 0 is, komt er 'sold out' vanbovenaan
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("SOLD OUT!");
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.WriteLine($"\nName: {productNames[secondChoice]}");
                            Console.WriteLine($"Quantity: {productQuantities[secondChoice]} Available.");
                            Console.WriteLine($"Price: €{productPrices[secondChoice]}");
                            Console.WriteLine($"Details: {productDetails[secondChoice]}");
                            Console.WriteLine($"Layout: {productLayouts[secondChoice]}\n");
                            Console.ResetColor();
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nThis product does not exist.\n");
                        Console.ResetColor();
                    }
                }
                #endregion einde van code voor optie 1

                #region begin code voor optie 2 (producten kopen)
                if (choice == 2)
                {
                    Console.Write("Which product would you like to purchase: ");
                    int secondChoice = Convert.ToInt32(Console.ReadLine());
                    secondChoice--;

                    if (secondChoice >= 0 && secondChoice < productNames.Length) //als user een input geeft tussen 1 en 5 (0-4 index)
                    {
                        Console.Write($"How many {productNames[secondChoice]}'s would you like to purchase: ");
                        int secondChoiceAmount = Convert.ToInt32(Console.ReadLine());
                        Console.Clear();
                        PurchaseProduct(secondChoice, secondChoiceAmount, productQuantities, vatPercentage, productPrices, productNames); //roept methode op
                    }
                    else
                    {
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nThis product does not exist.\n");
                        Console.ResetColor();
                    }
                }
                #endregion einde code voor optie 2

                #region begin code voor optie 3 (inloggen)
                if (choice == 3)
                {
                    if (!adminLogin) //als adminLogin false is
                    {
                        Console.Write("\nPassword: ");
                        string passwordInput = Console.ReadLine();
                        if (passwordInput == password)
                        {
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\nLog in successful.\n");
                            Console.ResetColor();
                            adminLogin = true;
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("Message from the developer");
                            Console.WriteLine("More functionality will be added in future updates!\n");
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("\nThe password is incorrect.\n");
                            Console.ResetColor();
                        }
                    }
                    else if (adminLogin) //als adminLogin true is
                    {
                        adminLogin = false;
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("\nYou've logged out\n");
                        Console.ResetColor();
                    }
                }
                #endregion einde code voor optie 3 (inloggen)

                #region begin code voor optie 4 (exit)
                if (choice == 4)
                {
                    exitProgram = true; //exit de main loop
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine($"\n{shopName}: Have a wonderful day {userName}!");
                    Console.Write("PRESS ENTER TO EXIT..");
                    Console.ResetColor();
                    Console.ReadKey();
                    Console.Clear();
                }
                #endregion einde code voor optie 4

                #region begin code voor optie 5 (refill)
                if (choice == 5 && adminLogin)
                {
                    Console.Clear();
                    DisplayProductInfo(productNames, productQuantities); //roept weer de methode op voor het shopping menu
                    Console.Write("\nChoose a product to restock: ");
                    int inputStockProduct = Convert.ToInt32(Console.ReadLine());

                    if (inputStockProduct >= 1 && inputStockProduct <= 5)
                    {
                        int inputToIndex = inputStockProduct - 1; //doet input in een nieuwe var en -1 omdat we telle vanaf 0

                        Console.Write($"How many {productNames[inputToIndex]}'s would you like to add? "); //dus als user 1 ingeeft word het productNames[0]
                        int inputStockFill = Convert.ToInt32(Console.ReadLine());
                        productQuantities[inputToIndex] += inputStockFill;

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"\nSuccessfully added {inputStockFill} {productNames[inputToIndex]}'s");
                        Console.WriteLine($"There's currently {productQuantities[inputToIndex]} {productNames[inputToIndex]}'s available\n");
                        Console.ResetColor();
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.Write("PRESS ENTER TO CONTINUE..");
                        Console.ResetColor();
                        Console.ReadKey();
                        Console.Clear();
                    }
                    else
                    {
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("This product does not exist.");
                        Console.ResetColor();
                    }
                }
                #endregion einde code voor optie 5

                if (choice < 1 || choice > 5) //heb ik toegevoegd moest een user een optie kleiner dan 1 of hoger dan 5 ingeven
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nThis option is not available.\n");
                    Console.ResetColor();
                }
            }
        }

        #region code voor alle methodes
        private static void PurchaseProduct(int secondChoice, int secondChoiceAmount, int[] productQuantities, int vatPercentage, double[] productPrices, string[] productNames) //code om de BTW en prijs te berekenen
        {
            Console.Clear();
            if (secondChoiceAmount > productQuantities[secondChoice])
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nFailed to purchase, there's only {productQuantities[secondChoice]} Available.\n");
                Console.ResetColor();
            }
            else
            {
                productQuantities[secondChoice] -= secondChoiceAmount;
                double priceWithoutVat = secondChoiceAmount * productPrices[secondChoice];
                priceWithoutVat = Math.Round(priceWithoutVat, 2);
                double vatAmount = productPrices[secondChoice] / 100 * vatPercentage * secondChoiceAmount;
                vatAmount = Math.Round(vatAmount, 2);
                double priceWithVat = priceWithoutVat + vatAmount;
                priceWithVat = Math.Round(priceWithVat, 2);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"\nYou've successfully purchased {secondChoiceAmount} {productNames[secondChoice]}('s) for €{priceWithVat}");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write($"Quantity after purchase: {productQuantities[secondChoice]}\n");
                Console.ResetColor();
                Console.WriteLine($"Price before {vatPercentage}% VAT: €{priceWithoutVat}");
                Console.WriteLine($"VAT amount: €{vatAmount}");
                Console.Write($"Price with {vatPercentage}% VAT: €{priceWithoutVat} => ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"€{priceWithVat}\n");
                Console.ResetColor();
            }
        }

        private static void DisplayProductInfo(string[] productNames, int[] productQuantities) //is de shopping menu zodat ik het niet dubbel hoef te schrijven
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(new string('-', 45));
            Console.WriteLine("we are selling the following products:");
            Console.ResetColor();
            for (int i = 0; i < productNames.Length; i++) //length is 5 dus loopt 5x, [i] is 0 en eindigt op 4, print alle productNames & Quantities af
            {
                Console.WriteLine($"Product {i + 1}: {productNames[i]} - {productQuantities[i]} Available.");
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(new string('-', 45));
            Console.ResetColor();
        }
        #endregion einde code voor alle methodes
    }
}
